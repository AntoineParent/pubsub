// subscriber store
let topics = {}

// example:
// topics = {topic1: [subscriber1, subscriber2], topic2: [subscriber3]}

let subscribe = (topic, subscriber) => {
    // initializes topic if not existent
    if (!topics[topic])
        topics[topic] = []

    // push subscriber to topic's listener
    topics[topic].push(subscriber)
}

let unsubscribe = (topic, subscriber) => {
    topics[topic].splice(topics[topic].indexOf(subscriber), 1);
}

let publish = (topic, option) => {
    // for each registered subscriber to given topic, call subscriber(option)
    topics[topic].forEach(subscriber => {
        subscriber(option)
    })

}

module.exports = { subscribe: subscribe, publish: publish, unsubscribe: unsubscribe }
