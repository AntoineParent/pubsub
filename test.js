let bus = require('./index')

function a(question) {
    if (question)
        bus.publish('question', question)
}

function b() {
    return 42
}

function karate(action) {
    if(action)
        bus.publish('action', action)
}


bus.subscribe('question', (question) => console.log(b(question)))
bus.subscribe('question', () => console.log('ça dépend'))

//bus.unsubscribe('question', (question) => console.log(b(question)))

//bus.subscribe('action', (action) => console.log(karate('Baaanzzzzzaaaïïïï')))

a("Quel est le sens de la vie, de l'univers et le reste ?")